import { Injectable } from '@angular/core';
import { Users } from './interface/users';
import { Observable, Subject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<Users | null>;
  private logInErrorSubject = new Subject<string>();

  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
  }


  constructor(public afAuth:AngularFireAuth) {
    this.user = this.afAuth.authState;
   }

   signup(email:string, passwoerd:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,passwoerd)
        .then(res => console.log('Successful Signup',res))  
        .catch (
          error => window.alert(error)
           )
  }

  logout(){
    this.afAuth.auth.signOut()
    .then(res => console.log('Successful logout',res));
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
          res => console.log('Succesful Login',res)
        ).catch (
          error => window.alert(error)
           )
       
  }

  
}
