import { User } from './interface/user';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from './interface/post';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  getposts():Observable<any[]>{
    return this.db.collection('posts').valueChanges({idField:'id'});
  }

  addPosts(body:string, author:string,name:string){
    const post = {body:body, author:author, name:name};
    this.db.collection('posts').add(post);
  }

  getpost(id:string):Observable<any>{
        return this.db.doc(`posts/${id}`).get();
  }

  updatePost(id:string,name:string,author:string,body:string){
    const post = {name:name,author:author,body:body};
    this.db.doc(`posts/${id}`).update(post);
  }
        
  deletePost(id:string){
    this.db.doc(`posts/${id}`).delete();
  }
  

  constructor(private _http: HttpClient,private db:AngularFirestore) { }

  /*getPost(){
    return this._http.get<Post[]>(this.apiurl);
  }
  */


 

  
 

  

}
