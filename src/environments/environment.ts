// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBLM8RTRL580rJuEoI2bpnE9F7l-nRxYgg",
    authDomain: "home-ex-f3449.firebaseapp.com",
    databaseURL: "https://home-ex-f3449.firebaseio.com",
    projectId: "home-ex-f3449",
    storageBucket: "home-ex-f3449.appspot.com",
    messagingSenderId: "1003858466189",
    appId: "1:1003858466189:web:37322e59813831934e19c4",
    measurementId: "G-SCMF5LVFSS"
  }
};




/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
